#ifndef __FILTER_H__
#define __FILTER_H__

#include "config.h"
#include <stdbool.h>

/**
 * Determines whether a packet should be blocked or forwarded.
 *
 * @param config The configuration to check the packet against.
 * @param pkt The packet to check then forward or drop.
 * @return true if the packet is allowed, false otherwise.
 */
bool should_accept_packet(FilterConfig *config, unsigned char *pkt);

#endif
