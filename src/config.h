//
// Created by me on 4/20/18.
//

#ifndef PROJECT3_CONFIG_H
#define PROJECT3_CONFIG_H

#include <stdbool.h>
#include <stdint.h>

/// Configuration information for filtering packets.
typedef struct {
  /// The local ip address
  uint32_t local_ip_address;

  /// Mask determining which part of the address is the network address and
  /// which part is the global address.
  uint32_t local_mask;

  /// Whether or not to block inbound ICMP ping.
  bool block_inbound_ping;

  /// Number of blocked inbound TCP ports.
  uint32_t blocked_tcp_ports_count;

  /// Blocked tcp ports.
  uint16_t *blocked_tcp_ports;

  /// Number of IP Addresses which are blocked.
  uint32_t blocked_addresses_count;

  /// Array of blocked addresses.
  uint32_t *blocked_addresses;
} FilterConfig;

/// Opens a configuration file, parses it into the configuration struct and
/// closes the configuration file. Prints error and returns null if any.
FilterConfig *make_filter_config_from_file(const char *file_path);

/// De-allocates the configuration.
void free_filter_config(FilterConfig *filterConfig);

/// This must be called before make_filter_config_from_file is called. It
/// compiles regexps used to read the configuration file.
void initialize_regexp();

/// Must be called after all calls to make_filter_config. Frees memory used by
/// the regexp.
void free_regexp();

/// Checks whether two filters are the same.
bool compare_filter(FilterConfig a, FilterConfig b);

#endif // PROJECT3_CONFIG_H
