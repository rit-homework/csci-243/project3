#define _DEFAULT_SOURCE
/**
 * Project 3:
 * Student: mxc4400
 */

#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "config.h"
#include "filter.h"
#include "filter_thread.h"

/// The thread key associated with the destructor for the filter thread.
static pthread_key_t filter_thread_key;

/// The filter thread instance.
static pthread_t filter_thread;

/**
 * Signal handler passes signal information to the child thread so that the
 * thread can gracefully terminate.
 *
 * @param signum The signal which was received.
 */
static void signal_handler(int signum) {
  if (signum == SIGHUP) {
    filter_thread_running = false;

    printf("\nfw: received Hangup request. Cancelling...\n");
    fflush(stdout);

    // Cancel the thread after a bit of a wait to allow it to clean up on its
    // own first. This is needed because the thread could be blocked on I/O.
    usleep(100);
    pthread_cancel(filter_thread);
  }
}

/**
 * Installs signal handlers.
 */
static void init_signal_handlers() {
  struct sigaction signal_action = {
      .sa_flags = 0,
      .sa_handler = signal_handler,
  };

  sigemptyset(&signal_action.sa_mask); // no masked interrupts
  sigaction(SIGHUP, &signal_action, NULL);
}

/// Displays a prompt to stdout and menu of commands that a user can choose
static void display_menu(void) {
  printf("\n\n1. Block All\n");
  printf("2. Allow All\n");
  printf("3. Filter\n");
  printf("0. Exit\n");
  printf("> ");
}

/**
 * A helper method to print display the control interface and handle user input.
 */
static void display_control_interface() {
  while (filter_thread_running) {
    display_menu();

    unsigned int choice;
    scanf("%ui", &choice);

    switch (choice) {
    case 1:
      filter_mode = (volatile FilterMode)MODE_BLOCK_ALL;
      printf("blocking all packets\n");
      break;

    case 2:
      filter_mode = (volatile FilterMode)MODE_ALLOW_ALL;
      printf("allowing all packets\n");
      break;

    case 3:
      filter_mode = (volatile FilterMode)MODE_FILTER;
      printf("filtering packets\n");
      break;

    case 0:
      filter_thread_running = false;
      usleep(100);
      pthread_cancel(filter_thread);
      break;

    default:
      break;
    }
  }
}

/**
 * Creates a filter, opens files and launches the user interface and filtering
 * thread.
 */
int main(int argc, char *argv[]) {
  if (argc < 2) {
    fprintf(stderr, "usage: %s configFileName\n", argv[0]);
    return EXIT_FAILURE;
  }

  init_signal_handlers();
  initialize_regexp();

  // Open Configuration File
  FilterConfig *config = make_filter_config_from_file(argv[1]);
  free_regexp();
  if (config == NULL) {
    return EXIT_FAILURE;
  }

  FILE *in = fopen("ToFirewall", "rb");
  if (in == NULL) {
    fprintf(stderr, "Failed to Open ToFirewall\n");
    return EXIT_FAILURE;
  }

  FILE *out = fopen("FromFirewall", "wb");
  if (out == NULL) {
    fprintf(stderr, "Failed to Open FromFirewall\n");
    return EXIT_FAILURE;
  }

  Args *args = malloc(sizeof(Args));
  args->out = out;
  args->in = in;
  args->config = config;

  printf("fw: starting filter thread.\n");
  pthread_key_create(&filter_thread_key,
                     (void (*)(void *))filter_thread_destructor);
  pthread_setspecific(filter_thread_key, &args);
  int ret = pthread_create(&filter_thread, NULL, run_filter_thread, args);
  if (ret != 0) {
    fprintf(stderr, "Failed to Create Filter Thread\n");
    return EXIT_FAILURE;
  }

  display_control_interface();

  printf("fw: main is joining the thread.\n");
  fflush(stdout);

  int joinResult = pthread_join(filter_thread, NULL);
  if (joinResult != 0) {
    printf("fw: main Error: unexpected joinResult: %d\n", joinResult);
    fflush(stdout);
  }
  filter_thread_destructor((void **)&args);
  printf("fw: main confirmed that the thread was canceled.\n");
  printf("fw: main returning.\n");
  fflush(stdout);
  return EXIT_SUCCESS;
}
