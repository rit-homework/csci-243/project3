#include "filter.h"
#include "config.h"
#include "pktUtility.h"

/**
 * Checks if traffic is allowed to go to (or from) an ip address.
 *
 * @param config The config to check against.
 * @param src_addr The source address.
 * @param dst_addr The destination address.
 * @return true if traffic is allowed to pass through the firewall, based on the
 * ip.
 */
static bool check_if_blocked_by_ip(FilterConfig *config, uint32_t src_addr,
                                   uint32_t dst_addr) {
  for (uint32_t i = 0; i < config->blocked_addresses_count; i++) {
    uint32_t *blocked_address = &config->blocked_addresses[i];

    if (src_addr == *blocked_address || dst_addr == *blocked_address) {
      return true;
    }
  }

  return false;
}

/**
 * A helper function to check whether a port is open.
 *
 * @param config The configuration specifying blocked ports.
 * @param port The port to check.
 * @return Whether the port is open and traffic can flow through it.
 */
static bool is_port_allowed(FilterConfig *config, uint16_t port) {
  for (uint32_t i = 0; i < config->blocked_tcp_ports_count; i++) {
    uint16_t blocked_port = config->blocked_tcp_ports[i];

    if (port == blocked_port) {
      return false;
    }
  }

  return true;
}

/**
 * Checks whether a packet is inbound.
 *
 * @param config The config which defines what the local network is.
 * @param src_addr The source address of the packet.
 * @param dst_addr The destination address of the packet.
 * @return true if the packet is inbound, false if it is local or outbound.
 */
static bool packet_is_inbound(FilterConfig *config, uint32_t src_addr,
                              uint32_t dst_addr) {
  if ((src_addr & config->local_mask) ==
      (config->local_ip_address & config->local_mask)) {
    // Traffic From Local Network To Local Network Isn't Inbound
    return false;
  }

  // Only Traffic From World
  if ((dst_addr & config->local_mask) !=
      (config->local_ip_address & config->local_mask)) {
    // Traffic To Outside Of Network (Outbound)
    return false;
  }

  return true;
}

bool should_accept_packet(FilterConfig *config, unsigned char *pkt) {
  uint32_t src_addr = ExtractSrcAddrFromIpHeader(pkt);
  uint32_t dst_addr = ExtractDstAddrFromIpHeader(pkt);

  bool blocked_by_ip = check_if_blocked_by_ip(config, src_addr, dst_addr);
  if (blocked_by_ip) {
    return false;
  }

  if (!packet_is_inbound(config, src_addr, dst_addr)) {
    // No Further Filtering On Outbound and Local Packets
    return true;
  }

  unsigned int protocol = ExtractIpProtocol(pkt);
  if (protocol == IP_PROTOCOL_TCP) {
    uint16_t dst_port = (uint16_t)ExtractTcpDstPort(pkt);
    bool allowed = is_port_allowed(config, dst_port);
    return allowed;
  }

  if (config->block_inbound_ping && protocol == IP_PROTOCOL_ICMP) {
    // Block Inbound Ping
    unsigned char type = ExtractIcmpType(pkt);
    if (type == ICMP_TYPE_ECHO_REQ) {
      return false;
    }
  }

  return true;
}
