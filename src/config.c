#define _DEFAULT_SOURCE
#include "config.h"
#include "pktUtility.h"
#include <malloc.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static regex_t config_line_regex;

void initialize_regexp() {
  // Compile Regexp
  int ret = regcomp(&config_line_regex, "^([A-Z_]+): (.*)$", REG_EXTENDED);
  if (ret != 0) {
    char buffer[1024];
    regerror(ret, &config_line_regex, buffer, sizeof(buffer));
    fprintf(stderr, "Could not compile regular expression (%d: %s).\n", ret,
            buffer);
    exit(1);
  }
}

void free_regexp() { regfree(&config_line_regex); }

/// A helper method which creates a 32bit netmask. It creates a number where the
/// mask_count most significant bits are 1s and the rest 0s.
static uint32_t make_netmask(unsigned int mask_count) {
  return 0xFFFFFFFF << (32 - mask_count);
}

/// Given an ip address with a netmask, as a string extracts that information.
static void get_ip_and_mask(char *str, uint32_t *address, uint32_t *mask,
                            unsigned int *mask_count_out) {
  unsigned char octets[4];
  unsigned int mask_count = 32;

  sscanf(str, "%hhu.%hhu.%hhu.%hhu/%u", &octets[0], &octets[1], &octets[2],
         &octets[3], &mask_count);
  *address = ConvertIpUCharOctetsToUInt(octets);
  *mask = make_netmask(mask_count);

  if (mask_count_out != NULL) {
    *mask_count_out = mask_count;
  }
}

/// Creates a heap allocated, null terminated string from a string and two
/// offsets.
char *get_sub_string(int start_offset, int end_offset, char *string) {
  size_t length = (end_offset + 1) - (start_offset + 1);
  char *sub_str = malloc(sizeof(char) * length + 1);

  strncpy(sub_str, &string[start_offset], length);
  sub_str[length] = '\0';

  return sub_str;
}

/// A helper method to allocate a filter configuration.
static FilterConfig *make_filter_config() {
  return calloc(1, sizeof(FilterConfig));
}

FilterConfig *make_filter_config_from_file(const char *file_path) {
  FilterConfig *config = make_filter_config();
  if (config == NULL) {
    return NULL;
  }

  FILE *file = fopen(file_path, "r");
  if (file == NULL) {
    fprintf(stderr, "ERROR: invalid config file\n");
    return NULL;
  }

  // Whether or not the local address configuration option has been visited.
  bool set_local = false;

  // A pointer to the buffer allocated by getline.
  char *line = NULL;

  // The size of the buffer allocated by getline.
  size_t len = 0;

  while (true) {
    // Read Each Line In (null terminated)
    ssize_t read = getline(&line, &len, file);
    if (read == -1) {
      break;
    }

    if (read == 1 && line[0] == '\n') {
      // Skip Empty Lines
      continue;
    }

    // Remove Newline
    if (line[read - 1] == '\n') {
      line[read - 1] = '\0';
    }

    if (strcmp("BLOCK_PING_REQ", line) == 0) {
      config->block_inbound_ping = true;
      continue;
    }

    size_t max_captures = 3;
    regmatch_t captures[max_captures];

    int ret = regexec(&config_line_regex, line, max_captures, captures, 0);
    if (ret != 0) {
      fprintf(stderr, "ERROR: invalid config file\n");
      free(line);
      free_filter_config(config);
      return NULL;
    }

    char *command_name =
        get_sub_string(captures[1].rm_so, captures[1].rm_eo, line);
    char *arg = get_sub_string(captures[2].rm_so, captures[2].rm_eo, line);

    if (strcmp("LOCAL_NET", command_name) == 0) {
      set_local = true;
      get_ip_and_mask(arg, &config->local_ip_address, &config->local_mask,
                      NULL);
    } else if (strcmp("BLOCK_INBOUND_TCP_PORT", command_name) == 0) {
      uint16_t tcp_port = (uint16_t)strtoll(arg, NULL, 10);
      if (tcp_port == 0) {
        fprintf(stderr, "ERROR: invalid config file: the tcp port is invalid.");

        free(line);
        free(arg);
        free(command_name);
        fclose(file);
        free_filter_config(config);
        return NULL;
      }

      config->blocked_tcp_ports_count++;
      config->blocked_tcp_ports =
          realloc(config->blocked_tcp_ports,
                  sizeof(uint16_t) * config->blocked_tcp_ports_count);
      config->blocked_tcp_ports[config->blocked_tcp_ports_count - 1] = tcp_port;
    } else if (strcmp("BLOCK_IP_ADDR", command_name) == 0) {
      uint32_t address;
      uint32_t mask;
      unsigned int mask_count;

      get_ip_and_mask(arg, &address, &mask, &mask_count);

      if (mask_count != 32) {
        fprintf(stderr, "ERROR: invalid config file: only ip addresses can be "
                        "blocked, not ranges\n");

        free(line);
        free(arg);
        free(command_name);
        fclose(file);
        free_filter_config(config);
        return NULL;
      }

      config->blocked_addresses_count++;
      config->blocked_addresses =
          realloc(config->blocked_addresses,
                  sizeof(uint32_t) * config->blocked_addresses_count);

      config->blocked_addresses[config->blocked_addresses_count - 1] = address;
    } else {
      fprintf(stderr, "ERROR: invalid config file\n");

      free(line);
      free(arg);
      free(command_name);
      fclose(file);
      free_filter_config(config);
      return NULL;
    }

    free(arg);
    free(command_name);
  }

  if (line != NULL) {
    free(line);
  }
  fclose(file);

  if (!set_local) {
    fprintf(stderr, "ERROR: configuration file must set LOCAL_NET\n");
    free_filter_config(config);
    return NULL;
  }

  return config;
}

void free_filter_config(FilterConfig *filterConfig) {
  free(filterConfig->blocked_tcp_ports);
  free(filterConfig->blocked_addresses);
  free(filterConfig);
}

/// A helper function to compare two arrays.
static bool compare_all_uint16(uint16_t *a, uint16_t *b, uint32_t count) {
  for (uint32_t i = 0; i < count; i++) {
    if (a[i] != b[i]) {
      return false;
    }
  }

  return true;
}

/// A helper function to compare two arrays.
static bool compare_all_uint32(uint32_t *a, uint32_t *b, uint32_t count) {
  for (uint32_t i = 0; i < count; i++) {
    if (a[i] != b[i]) {
      return false;
    }
  }

  return true;
}

bool compare_filter(FilterConfig a, FilterConfig b) {
  if (a.local_ip_address != b.local_ip_address ||
      a.local_mask != b.local_mask ||
      a.block_inbound_ping != b.block_inbound_ping ||
      a.blocked_tcp_ports_count != b.blocked_tcp_ports_count ||
      a.blocked_addresses_count != b.blocked_addresses_count) {
    return false;
  }

  // Compare Blocked Ports
  if (!compare_all_uint16(a.blocked_tcp_ports, b.blocked_tcp_ports,
                          a.blocked_tcp_ports_count)) {
    return false;
  }

  // Compare Blocked Addresses
  if (!compare_all_uint32(a.blocked_addresses, b.blocked_addresses,
                          a.blocked_addresses_count)) {
    return false;
  }

  return true;
}
