#include <stdio.h>
#include <stdlib.h>

#include "config.h"
#include "filter.h"
#include "filter_thread.h"

#define MAX_PACKET_LENGTH 2048

volatile bool filter_thread_running = true;

volatile FilterMode filter_mode = MODE_FILTER;

/**
 * The typed, argument implementation of the filter thread loop.
 *
 * @param args The args which are passed to the thread. Ownership is given to
 * the thread.
 */
static void run_filter_thread_impl(Args *args) {
  while (filter_thread_running) {
    uint32_t size = 0;
    size_t bytes_read = fread(&size, sizeof(uint32_t), 1, args->in);
    if (bytes_read == 0) {
      return;
    }

    unsigned char packet[size];
    bytes_read = fread(&packet, sizeof(packet), 1, args->in);
    if (bytes_read == 0) {
      return;
    }

    if (bytes_read > MAX_PACKET_LENGTH) {
      continue;
    }

    if (filter_mode == MODE_ALLOW_ALL ||
        (filter_mode == MODE_FILTER &&
         should_accept_packet(args->config, packet))) {
      fwrite(&size, sizeof(size), 1, args->out);
      fwrite(&packet, sizeof(packet), 1, args->out);
    }
  }

  printf("fw: thread returning. status: %d\n", EXIT_SUCCESS);
  fflush(stdout);
}

void *run_filter_thread(void *args) {
  run_filter_thread_impl(args);
  return NULL;
}

void filter_thread_destructor(void **raw_args) {
  if (*raw_args == NULL) {
    return;
  }

  Args *args = *raw_args;

  printf("fw: thread is deleting filter data.\n");
  printf("fw: thread destructor is deleting filter data\n");
  fflush(stdout);

  // Cleanup Owned Resources
  free_filter_config(args->config);

  printf("fw: thread destructor is closing pipes.\n");
  fflush(stdout);
  fclose(args->out);
  fclose(args->in);

  free(args);

  *raw_args = NULL;
}
