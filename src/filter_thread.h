#ifndef PROJECT3_FILTER_THREAD_H
#define PROJECT3_FILTER_THREAD_H

#include "config.h"
#include <stdbool.h>

typedef struct {
  FILE *in;
  FILE *out;
  FilterConfig *config;
} Args;

/// Whether or not the filter thread is running.
extern volatile bool filter_thread_running;

/// Type used to control the mode of the firewall.
typedef enum { MODE_BLOCK_ALL, MODE_ALLOW_ALL, MODE_FILTER } FilterMode;

/// Controls the mode of the filtering. This is updated by the main thread and
/// read by the filter thread.
extern volatile FilterMode filter_mode;

/**
 * Function passed to pthread_create to run the filtering thread.
 */
void *run_filter_thread(void *args);

/**
 * Function called to destroy resources owned by the thread.
 */
void filter_thread_destructor(void **args);

#endif // PROJECT3_FILTER_THREAD_H
