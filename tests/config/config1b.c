#include <assert.h>
#include <stdio.h>
#include "config.h"
#include "memory.h"

int main(int argc, char** argv) {
    initialize_regexp();

    FilterConfig* config = make_filter_config_from_file(argv[1]);
    uint32_t addrs[1] = {0x4A7D1567};

    FilterConfig expected = {
            .local_ip_address = 0x81152500,
            .local_mask = (0xFFFFFFFF << 8),
            .block_inbound_ping = false,
            .blocked_tcp_ports = NULL,
            .blocked_tcp_ports_count = 0,
            .blocked_addresses = addrs,
            .blocked_addresses_count = 1,
    };

    int ret = compare_filter(*config, expected) == true ? 0 : 1;
    free_filter_config(config);
    return ret;
}