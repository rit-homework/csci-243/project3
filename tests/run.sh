#!/bin/bash +x

set -e

REFERENCE_SOLUTION=../bin/referenceFirewall
FWSIM=../bin/fwSim
SOLUTION=../src/firewall
VALGRIND="valgrind --leak-check=full --show-leak-kinds=all --suppressions=valgrind.supp"

if [ ! -p ToFirewall ]; then
    mkfifo ToFirewall
fi

if [ ! -p FromFirewall ]; then
    mkfifo FromFirewall
fi

$FWSIM -i ./data/packets.1 -o ./outdata -d 1 $REFERENCE_SOLUTION ./config/config1.txt > expected.1.txt
$FWSIM -i ./data/packets.1 -o ./outdata -d 1 $SOLUTION ./config/config1.txt > actual.1.txt

diff expected.1.txt actual.1.txt

$FWSIM -i ./data/packets.3 -o ./outdata -d 1 $REFERENCE_SOLUTION ./config/config1.txt > expected.3.txt
$FWSIM -i ./data/packets.3 -o ./outdata -d 1 $SOLUTION ./config/config1.txt > actual.3.txt

diff expected.3.txt actual.3.txt
